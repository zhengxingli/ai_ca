% These actions are taken to reach the goal. 
% We placed actions which are most unlikely to be taken at first to prevent the program stepping into infinite loop.
action(state(X,Y),state(7,Z), OpenList, ClosedList) :- 
    Z is (X+Y)-7, Z>=0,
    not(member(state(7, Z), OpenList)),
    not(member(state(7, Z), ClosedList)),
    write("Fill the 7L jug from the 4L jug : " + [7,Z]), nl. 

action(state(X,Y),state(Z,4), OpenList, ClosedList) :- 
    Z is (X+Y)-4, Z>=0,
    not(member(state(Z, 4), OpenList)),
    not(member(state(Z, 4), ClosedList)),
    write("Fill the 4L jug from the 7L jug : " + [Z,4]), nl. 

action(state(X,Y),state(Z,0), OpenList, ClosedList) :- 
    Z is X+Y , Z=<7, Z >0,
    not(member(state(Z, 0), OpenList)),
    not(member(state(Z, 0), ClosedList)),
    write("Empty the 4L jug into the 7L jug : " + [Z,0]), nl.  

action(state(X,Y),state(0,Z), OpenList, ClosedList) :- 
    Z is X+Y, Z=<4, Z>0,
    not(member(state(0, Z), OpenList)),
    not(member(state(0, Z), ClosedList)),
    write("Empty the 7L jug into the 4L jug : " + [0,Z]), nl.

action(state(X,Y),state(0,Y), OpenList, ClosedList):- 
    X>0,
    not(member(state(0, Y), OpenList)),
    not(member(state(0, Y), ClosedList)),
    write("Empty 7L jug : "+ [0,Y]), nl .

action(state(X,Y),state(X,0), OpenList, ClosedList):- 
    Y>0,
    not(member(state(X,0),OpenList)),
    not(member(state(X,0), ClosedList)),
    write("Empty 4L jug : "+[X,0]), nl .

action(state(X,Y),state(7,Y), OpenList, ClosedList):- 
    X<7,
    not(member(state(7,Y),OpenList)),
    not(member(state(7,Y),ClosedList)),
    write("Fill 7L jug : "+[7,Y]), nl .

action(state(X,Y),state(X,4), OpenList, ClosedList):- 
    Y<4,
    not(member(state(X,4),OpenList)),
    not(member(state(X,4),ClosedList)),
    write("Fill 4L jug : "+[X,4]), nl .


% These actions are used to check whether the goal are reached or not.
action(state(X,Y),state(7,Z)) :- 
    Z is (X+Y)-7, Z>=0,
    write("Fill the 7L jug from the 4L jug : " + [7,Z]), nl. 

action(state(X,Y),state(Z,4)) :- 
    Z is (X+Y)-4, Z>=0,
    write("Fill the 4L jug from the 7L jug : " + [Z,4]), nl. 

action(state(X,Y),state(Z,0)) :- 
    Z is X+Y , Z=<7, Z >0,
    write("Empty the 4L jug into the 7L jug : " + [Z,0]), nl.  

action(state(X,Y),state(0,Z)) :- 
    Z is X+Y, Z=<4, Z>0,
    write("Empty the 7L jug into the 4L jug : " + [0,Z]), nl.

action(state(X,Y),state(0,Y)):- 
    X>0, 
    write("Empty 7L jug : "+ [0,Y]), nl .

action(state(X,Y),state(X,0)):- 
    Y>0,
    write("Empty 4L jug : "+[X,0]), nl .

action(state(X,Y),state(7,Y)):- 
    X<7,
    write("Fill 7L jug : "+[7,Y]), nl .

action(state(X,Y),state(X,4)):- 
    Y<4,
    write("Fill 4L jug : "+[X,4]), nl .


% Start looking for solutions
actions( OpenList, ClosedList) :- actions([state(0,0)],OpenList,ClosedList).

% Non-recursive part: it checks whether the goal is reached or not. 
% If the program reaches the goal, then print out solution and carry on searching for Solution. 
% '!' is used to prevent backtracking when solutions can be found.
actions([state(X0,Y0)|T],  OpenList, ClosedList) :- 
    action(state(X0,Y0),state(5,Y1)),
    nl,
    write("Solution: "+ [state(5,Y1),state(X0,Y0)|T]),
    nl,nl,
    !.

% Recursive part: This part follows a depth first search with open & closed list way to find solution.
% Add first candidate state to closed list and loop through all possible actions and add them to open list if they are not in both open and closed list.
actions([state(X0,Y0)|T],  OpenList, ClosedList) :-
    add(state(X0,Y0), ClosedList, NewClosedList),
    action(state(X0,Y0),state(X1,Y1), OpenList, ClosedList), 
    not(member(state(X1,Y1),[state(X0,Y0)|T])),
    add(state(X1,Y1), OpenList, NewOpenList),
    actions([state(X1,Y1),state(X0,Y0)|T],  NewOpenList, NewClosedList). 

% Start the program		
solve :- 
    intializeList(OpenList),
    intializeList(ClosedList),
    actions( OpenList, ClosedList), 
    nl,
    fail.

% Initalize an empty list.
intializeList([]).

% Add an element to a list.
add(X, S, S) :- member(X, S), !.
add(X, S, [X|S]).
   